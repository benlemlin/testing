$(document).ready(function () {
	$('#mul').click(mToggle);
	$('#div').click(dToggle);
	$('#add').click(aToggle);
	$('#sub').click(sToggle);
	$('#submit').click(startgame);
	$('#quiz').hide();
	$('#check').click(check);
	$('#next').click(next);
	$('#reset').click(reset);
})

var addition = false;
var subtraction = false;
var division = false;
var multiplication = false;
var noq = 0
var name = null;
var num1 = 0;
var num2 = 0;
var num3 = 0;
var num4 = 0;
var num5 = 0;
var num6 = 0;
var num7 = 0;
var symbol;
var question;
var solution;
var userAnswer;
var userScore = 0;
var questionNum = 1;
var percentage;

//collects data
var collect = function() {
	var difficulty = document.getElementsByName('difficulty');
	for (var i = 0, length = difficulty.length; i < length; i++)
	{
		if (difficulty[i].checked){
			level = difficulty[i].value
			break;
		}
	}
	name = $('#name').val();
	noq = $("#noq").val();
}

//toggles if user wants multiplication questions
var mToggle = function () {
		if (multiplication == false){
			multiplication = true;
		}
		else {
			multiplication = false;
		}
	}

//toggles if user wants division questions
var dToggle = function () {
		if (division == false){
			division = true;
		}
		else {
			division = false;
		}
		console.log(division);
	}

//toggles if user wants addition questions
var aToggle = function () {
		if (addition == false){
			addition = true;
		}
		else {
			addition = false;
		}
	}

//toggles if user wants subtraction questions
var sToggle = function () {
		if (subtraction == false){
			subtraction = true;
		}
		else {
			subtraction = false;
		}
	}

var operation = function() {
	var selector = Math.random();
	if (selector < 0.25 && multiplication == true) {
			symbol = " x ";
		}
	else if (selector < 0.5 && division == true) {
			symbol = " / ";
		}
	else if (selector < 0.75 && addition == true) {
			symbol = " + ";
		}
	else if (selector <= 1 && subtraction == true) {
			symbol = " - ";
		}
	else {
		operation();
	}
}

var questionGen = function() {
	operation();
	if (level == 1) {
		randomiser1();
		if (symbol == " x ") {
		question = num1 + " x " + num2;
		solution = num1 * num2;
	}
		else if (symbol == " / ") {
		question = num1 + " / " + num2;
		solution = (Math.round((num1 / num2) * 100) / 100);
	}
		else if (symbol == " + ") {
		question = num1 + " + " + num2;
		solution = num1 + num2;
	}
		else {
		question = num1 + " - " + num2;
		solution = num1 - num2;
	}
}
	else if (level == 2) {
		randomiser2();
		if (symbol == " x ") {
		question = num1 + " x " + num2;
		solution = num1 * num2;
	}
		else if (symbol == " / ") {
		question = num1 + " / " + num2;
		solution = (Math.round((num1 / num2) * 100) / 100);
	}
		else if (symbol == " + ") {
		question = num1 + " + " + num2;
		solution = num1 + num2;
	}
		else {
		question = num1 + " - " + num2;
		solution = num1 - num2;
	}
}
	else {
		randomiser3();
		if (symbol == " x ") {
		question = num1 + " x " + num2 + " x " + num3;
		solution = num1 * num2 * num3;
	}
		else if (symbol == " / ") {
		question = num1 + " / " + num2 + " / " + num3;
		solution = (Math.round((num1 / num2 / num3) * 100) / 100);
	}
		else if (symbol == " + ") {
		question = num1 + " + " + num2 + " + " + num3;
		solution = num1 + num2 + num3;
	}
		else {
		question = num1 + " - " + num2 + " - " + num3;
		solution = num1 - num2 - num3;
	}
	}
	console.log(question);
	console.log(solution);
	$('#question').html(question);

}

var randomiser1 = function () {
	num1 = Math.floor((Math.random() * 10) + 1);
	num2 = Math.floor((Math.random() * 10) + 1);
}

var randomiser2 = function() {
	num1 = Math.floor((Math.random() * 100) + 1);
	num2 = Math.floor((Math.random() * 100) + 1);
}

var randomiser3 = function() {
	num1 = Math.floor((Math.random() * 100) + 1);
	num2 = Math.floor((Math.random() * 100) + 1);
	num3 = Math.floor((Math.random() * 100) + 1);
}

var percent = function() {
	percentage = Math.round((userScore / noq) * 100);
}

var reset = function () { 
    window.location.reload();
};

var startgame = function() {
	collect();
	if (name != "") {
		if (multiplication == true || division == true || subtraction == true || addition == true) {
			if (noq >= 1 && noq <= 100){
				collect();
				questionGen();
				$('#settings').hide();
				$('#quiz').show();
				$('#next').hide();
				$('#qNumber').html("Question " + questionNum + " of " + noq);
				$('#reset').hide();
			}
			else {
				alert("Number of questions needs to be between 1 and 100");
			}
		}
		else {
			alert("Select at least one type of question");
		}
	}
	else {
		alert("Make sure to write your name");
	}
}

var check = function() {
	userAnswer = $('#userAnswer').val();
	if (userAnswer == solution) {
		$('#msg').show();
		$('#msg').html("That's Correct");
		$('#next').show();
		userScore ++;
		$('#score').show();
		$('#score').html("Your current score is " + userScore  + " / " + questionNum);
	}
	else {
		$('#msg').show();
		$('#msg').html("Not quite");
		$('#next').show();
		$('#score').show();
		$('#score').html("Your current score is " + userScore + " / " + questionNum);
	}
}

var next = function() {
	questionNum ++;
	if (questionNum > noq) {
		percent();
		$('#qNumber').hide();
		$('#userAnswer').hide();
		$('#check').hide();
		$('#question').hide();
		$('#next').hide();
		$('#msg').html("That's the end of the quiz.")
		$('#score').html(" Your score is " + userScore + " / " + noq + ". That's " + percentage + "%. Nice work " + name + "!!")
		$('#reset').show();
	}
	else {
	$('#userAnswer').val('')
	$('#msg').hide();
	$('#next').hide();
	$('#score').hide();
	$('#qNumber').html("Question " + questionNum + " of " + noq)
	questionGen();
	}
}





